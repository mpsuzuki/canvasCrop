  var CpioOldAsciiArchive = function() {
    this.members = new Array();
    this.headers = new Array();
    this.trailer = this.genHeader("TRAILER!!!", 0, new Uint8Array(0), false);
    return this;
  }

  CpioOldAsciiArchive.prototype.genHeader = function(filename, mDate, uint8Buff, isDirectory) {
    var magic =  "070707";
    var dev =    "000000";
    var ino =    "000000";
    var mode =   "100400"; // regular file, u=r,g=,o=
    var uid =    "000000";
    var gid =    "000000";
    var n_link = "000001"; // if directory, set to 2.
    var rdev =   "000000";
    if (mDate == null || mDate == undefined) {
      var mtime = ("00000000000" + parseInt((new Date)/1000).toString(8)).slice(-11);
    } else {
      var mtime = ("00000000000" + parseInt(mDate/1000).toString(8)).slice(-11);
    }
    var namesize = ("000000" + (filename.length + 1).toString(8)).slice(-6);
    var filesize = ("00000000000" + uint8Buff.length.toString(8)).slice(-11);

    if (isDirectory) {
      mode   = "400500"; // directory, u=rx,g=,o=
      n_link = "000002"; // ???
      filesize = "00000000000";
    }

    var asciiPart = magic + dev + ino + mode
                  + uid + gid + n_link + rdev
                  + mtime + namesize + filesize;

    var header = new Uint8Array(asciiPart.length + filename.length + 1);
    for (var i = 0; i < asciiPart.length; i += 1 ) {
      header[i] = asciiPart.charCodeAt(i);
    }
    for (var i = 0; i < filename.length; i += 1) {
      header[asciiPart.length + i] = filename.charCodeAt(i);
    }
    header[asciiPart.length + filename.length] = 0;

    return header;
  }

  CpioOldAsciiArchive.prototype.addMember = function(filename, mDate, uint8Buff, isDirectory) {
    var hdr = this.genHeader(filename, mDate, uint8Buff, isDirectory);
    this.headers.push(hdr);
    this.members.push(uint8Buff);
    return this;
  }

  CpioOldAsciiArchive.prototype.byteLength = function() {
    var byteLength = 0;
    for (var i = 0; i < this.members.length; i += 1) {
      byteLength += this.headers[i].length;
      byteLength += this.members[i].length;
    }
    byteLength += this.trailer.length;
    return byteLength;
  }

  CpioOldAsciiArchive.prototype.getArchiveUint8 = function() {
    var archiveLength = this.byteLength();
    var archiveBuff = new Uint8Array(archiveLength);

    var i = 0;
    for (var j = 0; j < this.members.length; j += 1) {
      for (var k = 0; k < this.headers[j].length; k += 1) {
        archiveBuff[i] = this.headers[j][k];
        i += 1;
      }
      for (var k = 0; k < this.members[j].length; k += 1) {
        archiveBuff[i] = this.members[j][k];
        i += 1;
      }
    }

    for (var j = 0; j < this.trailer.length; j += 1) {
      archiveBuff[i] = this.trailer[j];
      i += 1;
    }

    return archiveBuff;
  }

  CpioOldAsciiArchive.prototype.getArchiveBlob = function(genBlobFunc) {
    var blobDataArr = new Array();

    for (var j = 0; j < this.members.length; j += 1) {
      blobDataArr.push(this.headers[j]);
      blobDataArr.push(this.members[j]);
    }
    blobDataArr.push(this.trailer);

    return (genBlobFunc(blobDataArr, "application/x-cpio"));
  }
